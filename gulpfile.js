var gulp = require('gulp'),
	
	// Styles
	less = require('gulp-less'),
	lessAutoprefix = require('less-plugin-autoprefix'),
	cleanCSS = require('gulp-clean-css'),
	uncss = require('gulp-uncss'),
	minifyCssNames = require('gulp-minify-cssnames'),

	// JS
	concat = require('gulp-concat'),
	uglyfly = require('gulp-uglyfly'),

	// Image
	imagemin = require('gulp-imagemin'),

	// Code

	// System
	cache = require('gulp-cache'),
	del = require('del'),
	gulpIf = require('gulp-if')

;



/* Gulp Watcher */
gulp.task('watch', function() {

	gulp.watch('./app/static/styles/**/*.less', gulp.series(['less']));
	gulp.watch('./app/**/*.php', gulp.series(['move-code', 'compress-code']));
	gulp.watch('./app/static/js/**/*.js', gulp.series(['js']));
	gulp.watch('./app/static/imgs/**/*.*', gulp.series('image'));
	gulp.watch('./app/static/fonts/**/*.*', gulp.series('move-fonts'));

});


/* Remove 'dist' folder */ 
gulp.task('delete', () => {

	return del([
		'dist/**/*', 
		'!dist/imgs', 
		'!dist/imgs/**/*'
	]);

});


/* LESS -> CSS */
gulp.task('less', () => {

	return gulp.src('./app/static/styles/*.less')
		.pipe(less({
			plugins: [new lessAutoprefix({ browsers: ['last 2 versions'] })]
		}))
		.pipe(gulp.dest('./dist/static/styles'));

});


/* CSS */
gulp.task('compress-css', () => {

	return gulp.src([
			'./dist/static/styles/*.css',
		])
		.pipe(uncss({
            ignore: [
            	/\.owl/,
            	/\.datepicker/,
            	/\._menu_visible/,
            	/\._hidden/,
            	/\._active/,
            ],
            html: [
            	'./dist/**/*.php',
            ]
        }))
		.pipe(cleanCSS())
        .pipe(gulp.dest('./dist/static/styles'));

});


/* Minify CSS names */
gulp.task('minify-css-names', () => {

	return gulp.src([
			'./dist/**/*.+(php|js|css)',
		])
	    .pipe(minifyCssNames({
	    	prefix: '---',
	    	postfix: '',
	    }))
	    .pipe(gulp.dest('./dist'));

});


/* Optimize JS */
gulp.task('js', () => {

	gulp.src('./app/static/js/html_js.min.js')
		.pipe(gulp.dest('./dist/static/js'));

	gulp.src('./app/static/js/plugins/font_face_observer.min.js')
		.pipe(gulp.dest('./dist/static/js'));

	return gulp.src([
			'./app/static/js/plugins/image_lazyload.min.js',
			'./app/static/js/plugins/jquery-3.4.0.js',
			'./app/static/js/plugins/jquery.datepicker.js',
			'./app/static/js/plugins/jquery.owl-carousel.js',
			'./app/static/js/plugins/jquery.mask.js',
			'./app/static/js/script.js'
		])
		.pipe(concat('scripts.min.js'))
		.pipe(uglyfly())
		.pipe(gulp.dest('./dist/static/js'));

});


/* Optimize images */
gulp.task('image', () => {

	return gulp.src('./app/static/imgs/**/*.*')
		// .pipe(cache(imagemin([

		// 	imagemin.gifsicle({interlaced: true}),
		//     imagemin.jpegtran({progressive: true}),
		//     imagemin.optipng({optimizationLevel: 9}),
		//     imagemin.svgo({
		//         plugins: [
		//             {removeViewBox: true},
		//             {cleanupIDs: false}
		//         ]
		//     })

		// ])))
		.pipe(gulp.dest('./dist/static/imgs'));

});


/* Move fonts */
gulp.task('move-fonts', function() {

	return gulp.src('./app/static/fonts/**/*')
    	.pipe(gulp.dest('./dist/static/fonts'));

});


/* Move code */
gulp.task('move-code', function() {

	return gulp.src([
			'./app/**/*.php',
			'./app/.htaccess'
		])
		.pipe(gulp.dest('./dist'));

});


/* Compress code */
gulp.task('compress-code', function() {

	return gulp.src([
			'./dist/**/*.php',
		])
		.pipe(gulp.dest('./dist'));

});


/* Run Gulp */
gulp.task('default', gulp.series('delete', 'move-fonts', 'move-code', 'compress-code', 'image', 'js', 'less', 'watch'));