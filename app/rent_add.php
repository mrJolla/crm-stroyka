<?php require_once('layouts/header.php'); ?>
    <div class="---page-rent-in ---row">

        <div class="---col-12 ---col-lg-6 ---form">
            
            <div class="h1 ---font-light">Бронирование оборудования / № 2345 5424</div>

            <div class="---devider"></div>

            <div class="---data-block">
                <div class="h2 ---font-sbold">Данные арендатора</div>
                
                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Вид аренды</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Скидка на аренду</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Начало аренды</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Окончание аренды</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="---data-block">
                <div class="h2 ---font-sbold ---d-flex ---flex-wrap ---align-items-center ---justify-content-between">
                    Данные арендатора

                    <a href="#" class="---check-by-bailiff">Проверить через сервис приставов</a>
                </div>
                
                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6">
                        <label>ФИО клиента</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-sm-6">
                        <label></label>
                        <div class="---add-client-from-base ---pos-rel ---radius-5">
                            Добавить клиента из базы
                            <i class="ifont ---icon-loop ---y-pos-abs"></i>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Электронная почта</label>
                        <input type="email" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Номер телефона</label>
                        <input type="tel" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Номер и серия паспорта</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Номер и серия паспорта</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Дата выдачи паспорта</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Дата выдачи паспорта</label>
                        <input class="---input">
                    </div>

                    <div class="---field ---col-12">
                        <div class="---upload-passport ---radius-5 ---block--bg-white ---d-flex ---flex-wrap ---align-items-center ---justify-content-between">
                            <div class="---pass__info ---pos-rel">
                                <i class="ifont ---icon-camera ---y-pos-abs ---radius-5"></i>
                                <div class="h3">
                                    Отсканировать<br> паспорт
                                </div>
                            </div>
                            <div class="---d-flex ---btns-wrapper">
                                <div class="---col-12 ---col-xs-6">
                                    <label class="---input--file-upload ---radius-5">
                                        <input type="file" class="---d-none">
                                        <span class="---text-wrapper ---radius-5 ---font-sbold ---d-flex ---align-items-center">
                                            <span class="---input__file-name">Страница<br> с фото</span>
                                        </span>
                                    </label>
                                </div>
                                <div class="---col-12 ---col-xs-6">
                                    <label class="---input--file-upload ---radius-5">
                                        <input type="file" class="---d-none">
                                        <span class="---text-wrapper ---radius-5 ---font-sbold ---d-flex ---align-items-center">
                                            <span class="---input__file-name">Страница<br> с пропиской</span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="---btns-wrapper">
                <a href="#" class="---btn ---btn--fill-acent">Создать заявку</a>
            </div>

        </div>
        <div class="---col-12 ---col-lg-6">
            
            <div class="---r-side-add-block ---equipment-wrapper ---block--bg-white ---radius-5 ---form">
                <div class="h2 ---font-sbold">Запрашиваемое оборудование</div>

                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6">
                        <label>Данные в период от</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6">
                        <label>Данные в период до</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="---devider ---devider-mt0"></div>

                <div class="---prod-list">
                    <?php for($i = 0; $i < 3; $i++): ?>
                    <a href="#" class="---prod-item ---pos-rel ---d-flex ---align-items-center ---justify-content-between">
                        <span class="---prod-item__info ---d-inline-flex ---align-items-center">
                            <span class="---prod-item__img ---radius-5 ---d-flex ---align-items-center ---justify-content-center">
                                <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                            </span>
                            <span class="---prod-item__title ---font-sbold">Отбойный молоток<br> MAKITA HM1203C</span>
                        </span>

                        <span class="---prod-item__thumb ---d-inline-flex">
                            <span class="---thumb-item">
                                За единицу<br> 
                                <span class="---font-sbold">750 ₽</span>
                            </span>
                            <span class="---thumb-item">
                                В наличии<br> 
                                <span class="---font-sbold">0 шт.</span>
                            </span>
                        </span>

                        <span class="---prod-qnty-wrapper ---radius-5 ---d-inline-flex ---align-items-center">
                            <span class="---switcher ---d-inline-flex ---justify-content-center ---align-items-center">-</span>
                            <span class="---qnty ---font-sbold">1</span>
                            <span class="---switcher ---d-inline-flex ---justify-content-center ---align-items-center ---plus">+</span>
                        </span>
                    </a>
                    <?php endfor; ?>
                    
                    <a href="#" class="---btn ---btn--fill-dark">Добавить</a>
                </div>

                <div class="---devider"></div>
                
                <div class="---total-price ---font-sbold h2">
                    <div class="---delivery ---font-light">Доставка: 0 ₽</div>
                    Итого: 6 000 ₽
                </div>

                <div class="---devider"></div>

                <a href="#" class="---btn ---btn--fill-acent">Добавить платеж</a>

            </div>

        </div>

    </div>
<?php require_once('layouts/footer.php'); ?>