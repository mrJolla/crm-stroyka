<?php require_once('layouts/header.php'); ?>
    
    <div class="---page-clients">

        <div class="---tabs ---tabs--finance">
            <div class="---tabs__switchers-wrapper ---d-flex ---align-items-center ---justify-content-between">
                <div class="---tabs__switchers ---h1 ---font-light ---d-flex">
                    <div class="---tabs__switcher ---is-active" data-tab="1">Все клиенты</div>
                    <div class="---tabs__switcher" data-tab="2">Юр. лица</div>
                    <div class="---tabs__switcher" data-tab="3">Физ. лица</div>
                </div>
            </div>

            <div class="---h-filters-wrapper ---d-flex ---justify-content-between ---align-items-start">
                    
                <div class="---d-flex ---form ---align-items-start">
                    <a href="#" class="---filters ---js-toggle-show ---radius-5 ---d-inline-flex ---align-items-center" data-block="#filter">
                        Фильтры
                        <i class="ifont ---icon-menu-finance ---d-none ---sm-d-flex"></i>
                    </a>

                    <a href="#" class="---filters ---js-toggle-show ---radius-5 ---d-inline-flex ---align-items-center" data-block="#active-field">
                        Активные поля
                        <i class="ifont ---icon-list ---d-none ---sm-d-flex"></i>
                    </a>

                    <div class="---search ---field ---pos-rel">
                        <input class="---input" placeholder="Поиск по клиентам">
                        <button class="---y-pos-abs ifont ---icon-loop"></button>
                    </div>
                </div>

                <a href="#" class="---btn ---btn--fill-acent">Добавить товар</a>

            </div>

            <div id="filter" class="---d-none ---block--bg-white ---block-filter ---radius-5 ---form">
                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---btns ---d-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Применить фильтры</a>
                    <a href="#" class="---btn ---btn--border-bg">
                        Сбросить фильры
                        <i class="ifont ---icon-close"></i>
                    </a>
                </div>
            </div>

            <div id="active-field" class="---d-none ---block--bg-white ---block-filter ---radius-5 ---form">
                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---btns ---d-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Применить фильтры</a>
                    <a href="#" class="---btn ---btn--border-bg">
                        Сбросить фильры
                        <i class="ifont ---icon-close"></i>
                    </a>
                </div>
            </div>

            <div class="---tabs__tab ---is-visible" data-tab="1">
                
                <div class="---t-wrapper">
                    <table>
                        <tr>
                            <th><label class="---checkbox"></label></th>
                            <th>юр. лицо</th>
                            <th>номер телефона</th>
                            <th>Статус</th>
                            <th>Добавлен</th>
                            <th>Последний конт.</th>
                            <th>Источник</th>
                            <th>Прокатов</th>
                            <th>Доход</th>
                            <th>Скидка</th>
                        </tr>
                        <?php for($i = 0; $i < 2; $i++): ?>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>ООО Алтын Групп</td>
                            <td>+7 (999) 156-1992</td>
                            <td>
                                <div class="---select ---purple ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>18.04.2018</td>
                            <td>23.02.2019</td>
                            <td>Сайт</td>
                            <td>89</td>
                            <td><div class="---font-sbold">47 500 ₽</div></td>
                            <td><div class="---font-sbold">10 %</div></td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>ООО Алтын Групп</td>
                            <td>+7 (999) 156-1992</td>
                            <td>
                                <div class="---select ---dark ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>18.04.2018</td>
                            <td>23.02.2019</td>
                            <td>Сайт</td>
                            <td>89</td>
                            <td><div class="---font-sbold">47 500 ₽</div></td>
                            <td><div class="---font-sbold">10 %</div></td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>ООО Алтын Групп</td>
                            <td>+7 (999) 156-1992</td>
                            <td>
                                <div class="---select ---dark-red ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>18.04.2018</td>
                            <td>23.02.2019</td>
                            <td>Сайт</td>
                            <td>89</td>
                            <td><div class="---font-sbold">47 500 ₽</div></td>
                            <td><div class="---font-sbold">10 %</div></td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>ООО Алтын Групп</td>
                            <td>+7 (999) 156-1992</td>
                            <td>
                                <div class="---select ---light-red ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>18.04.2018</td>
                            <td>23.02.2019</td>
                            <td>Сайт</td>
                            <td>89</td>
                            <td><div class="---font-sbold">47 500 ₽</div></td>
                            <td><div class="---font-sbold">10 %</div></td>
                        </tr>
                        <?php endfor; ?>
                    </table>
                </div>
                
                <div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">

                    <div class="---nums ---d-flex ---align-items-center ---justify-content-between ---sm-justify-content-start">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            <span class="---d-none ---xs-d-block">Далее</span>
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>

                    <div class="---pagination__total-count">
                        Всего платежей — <span class="---font-sbold">8 521</span>
                    </div>
                </div>

            </div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>