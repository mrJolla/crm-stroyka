<?php require_once('layouts/header.php'); ?>
    
    <div class="---page-reports">

        <div class="---tabs ---tabs--finance">
            <div class="---tabs__switchers-wrapper ---d-flex ---align-items-center ---justify-content-between">
                <div class="---tabs__switchers ---h1 ---font-light ---d-flex">
                    <div class="---tabs__switcher ---is-active" data-tab="1">По количеству заказов</div>
                    <div class="---tabs__switcher" data-tab="2">По прибыли</div>
                    <div class="---tabs__switcher" data-tab="3">По сроку аренды</div>
                </div>

                <div class="---select">
                    <select>
                        <option value="">Закрытые 1</option>
                        <option value="">Закрытые 2</option>
                        <option value="">Закрытые 3</option>
                        <option value="">Закрытые 4</option>
                        <option value="">Закрытые 5</option>
                    </select>

                    <div class="---select__cur-value">
                        <span>Закрытые</span>
                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                    </div>

                    <div class="---select__list-wrapper">
                        <div class="---select__list">
                            <a href="#" class="---select__list-item">Закрытые 1</a>
                            <a href="#" class="---select__list-item">Закрытые 2</a>
                            <a href="#" class="---select__list-item">Закрытые 3</a>
                            <a href="#" class="---select__list-item">Закрытые 4</a>
                            <a href="#" class="---select__list-item">Закрытые 5</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="---tabs__tab ---is-visible" data-tab="1">
                <div class="---block-catalog">
                    <div class="---cat-wrapper ---row ---_xs">

                        <?php for($i = 0; $i < 8; $i++): ?>
                        <div class="---col col">
                            <a href="#" class="---cat-item ---d-flex ---flex-column">
                                <span class="---cat-item__img">
                                    <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                                </span>
                                <span class="---cat-item__title">
                                    Отбойный молоток MAKITA HM1203C
                                </span>
                                <span class="---cat-item__orders">
                                    <span class="---font-sbold">864</span> заказаов
                                </span>
                                <span class="---cat-item__btn ---font-sbold ---btn ---btn--sm">
                                    <i class="ifont ---icon-basket-linear"></i>
                                    от 750 ₽
                                </span>
                            </a>
                        </div>
                        <?php endfor; ?>

                    </div>
                </div>
                
                <div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">

                    <div class="---nums ---d-flex ---align-items-center ---justify-content-between ---sm-justify-content-start">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            <span class="---d-none ---xs-d-block">Далее</span>
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>

                    <div class="---pagination__total-count">
                        Всего платежей — <span class="---font-sbold">8 521</span>
                    </div>
                </div>
            </div>

            <div class="---tabs__tab" data-tab="2">
                <div class="---block-catalog">
                    <div class="---cat-wrapper ---row ---_xs">

                        <?php for($i = 0; $i < 8; $i++): ?>
                        <div class="---col col">
                            <a href="#" class="---cat-item ---d-flex ---flex-column">
                                <span class="---cat-item__img">
                                    <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                                </span>
                                <span class="---cat-item__title">
                                    Отбойный молоток MAKITA HM1203C
                                </span>
                                <span class="---cat-item__orders">
                                    <span class="---font-sbold">864</span> заказаов
                                </span>
                                <span class="---cat-item__btn ---font-sbold ---btn ---btn--sm">
                                    <i class="ifont ---icon-basket-linear"></i>
                                    от 750 ₽
                                </span>
                            </a>
                        </div>
                        <?php endfor; ?>

                    </div>
                </div>
                
                <div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">

                    <div class="---nums ---d-flex ---align-items-center ---justify-content-between ---sm-justify-content-start">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            <span class="---d-none ---xs-d-block">Далее</span>
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>

                    <div class="---pagination__total-count">
                        Всего платежей — <span class="---font-sbold">8 521</span>
                    </div>
                </div>
            </div>

            <div class="---tabs__tab" data-tab="3">
                <div class="---block-catalog">
                    <div class="---cat-wrapper ---row ---_xs">

                        <?php for($i = 0; $i < 8; $i++): ?>
                        <div class="---col col">
                            <a href="#" class="---cat-item ---d-flex ---flex-column">
                                <span class="---cat-item__img">
                                    <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                                </span>
                                <span class="---cat-item__title">
                                    Отбойный молоток MAKITA HM1203C
                                </span>
                                <span class="---cat-item__orders">
                                    <span class="---font-sbold">864</span> заказаов
                                </span>
                                <span class="---cat-item__btn ---font-sbold ---btn ---btn--sm">
                                    <i class="ifont ---icon-basket-linear"></i>
                                    от 750 ₽
                                </span>
                            </a>
                        </div>
                        <?php endfor; ?>

                    </div>
                </div>
                
                <div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">

                    <div class="---nums ---d-flex ---align-items-center ---justify-content-between ---sm-justify-content-start">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            <span class="---d-none ---xs-d-block">Далее</span>
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>

                    <div class="---pagination__total-count">
                        Всего платежей — <span class="---font-sbold">8 521</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="---col-12 ---devider-line"></div>

        <div class="---block ---block-index-stat ---row">

        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/1.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Конверсия<br> 
                    сайта
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    4,5%
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/2.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Новых<br> 
                    клиентов
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    19
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/3.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Постоянных<br> 
                    клиентов
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    73%
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/4.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Просроченные<br> 
                    в аренде
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    16
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/5.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    В ожидании<br> 
                    оплаты
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    7
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/6.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Новых заявок<br> 
                    за сегодня
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    22
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>

    </div>

        <div class="---col-12 ---devider-line"></div>
        
        <div class="---fin-report">
            <div class="h2 ---font-sbold">Финансовые отчеты</div>

            <div class="---report-item ---radius-5 ---d-flex ---align-items-center ---justify-content-between">
                <div class="---report__info">
                    <div class="---report__title ---font-sbold">Отчет о продажах</div>
                    <div class="---date">Последнее скачивание 26.04.2019</div>
                </div>

                <div class="---form ---report__period ---d-inline-flex ---align-items-center">
                    <span>Период с</span>
                    <div class="---field">
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <span>до</span>
                    <div class="---field">
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="---select">
                    <select>
                        <option value="">Закрытые 1</option>
                        <option value="">Закрытые 2</option>
                        <option value="">Закрытые 3</option>
                        <option value="">Закрытые 4</option>
                        <option value="">Закрытые 5</option>
                    </select>

                    <div class="---select__cur-value">
                        <span>Закрытые</span>
                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                    </div>

                    <div class="---select__list-wrapper">
                        <div class="---select__list">
                            <a href="#" class="---select__list-item">Закрытые 1</a>
                            <a href="#" class="---select__list-item">Закрытые 2</a>
                            <a href="#" class="---select__list-item">Закрытые 3</a>
                            <a href="#" class="---select__list-item">Закрытые 4</a>
                            <a href="#" class="---select__list-item">Закрытые 5</a>
                        </div>
                    </div>
                </div>

                <div class="---report__icons ---d-inline-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Создать</a>
                    <a href="#" class="ifont ---icon-upload"></a>
                    <a href="#" class="ifont ---icon-print"></a>
                </div>
            </div>
            <div class="---report-item ---radius-5 ---d-flex ---align-items-center ---justify-content-between">
                <div class="---report__info">
                    <div class="---report__title ---font-sbold">Отчет о продажах</div>
                    <div class="---date">Последнее скачивание 26.04.2019</div>
                </div>

                <div class="---form ---report__period ---d-inline-flex ---align-items-center">
                    <span>Период с</span>
                    <div class="---field">
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <span>до</span>
                    <div class="---field">
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="---select">
                    <select>
                        <option value="">Закрытые 1</option>
                        <option value="">Закрытые 2</option>
                        <option value="">Закрытые 3</option>
                        <option value="">Закрытые 4</option>
                        <option value="">Закрытые 5</option>
                    </select>

                    <div class="---select__cur-value">
                        <span>Закрытые</span>
                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                    </div>

                    <div class="---select__list-wrapper">
                        <div class="---select__list">
                            <a href="#" class="---select__list-item">Закрытые 1</a>
                            <a href="#" class="---select__list-item">Закрытые 2</a>
                            <a href="#" class="---select__list-item">Закрытые 3</a>
                            <a href="#" class="---select__list-item">Закрытые 4</a>
                            <a href="#" class="---select__list-item">Закрытые 5</a>
                        </div>
                    </div>
                </div>

                <div class="---report__icons ---d-inline-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Создать</a>
                    <a href="#" class="ifont ---icon-upload"></a>
                    <a href="#" class="ifont ---icon-print"></a>
                </div>
            </div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>