<?php require_once('layouts/header.php'); ?>

    <div class="---page-sets">
        
        <div class="h1 ---font-light">Создания и удаление наборов</div>

        <div class="---row">

            <div class="---col-xl-8">
                <div class="---docs-wrapper">
                    <div class="h2 ---font-sbold">Документы с арендатором</div>

                    <div class="---row">
                        
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="---docs-wrapper">
                    <div class="h2 ---font-sbold">Филиалы</div>

                    <div class="---row">
                        
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="---docs-wrapper">
                    <div class="h2 ---font-sbold">Статус заявки</div>

                    <div class="---row">
                        
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                
                <div class="---docs-wrapper">
                    <div class="h2 ---font-sbold">Статус пользователя</div>

                    <div class="---row">
                        
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="---col-lg-6">
                            <div class="---doc-item ---pos-rel ---radius-5">
                                <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                                <div class="---date">Обновлен 26.04.2019</div>

                                <div class="---icons ---y-pos-abs ---d-flex">
                                    <div class="---color ---radius-5" style="background: #000000;"></div>
                                    <a href="#" class="ifont ---icon-edit ---acent"></a>
                                    <a href="#" class="ifont ---icon-close"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="---col-xl-4 ---rside">
                <div class="---add-new-field ---radius-5">
                    <div class="h2 ---pos-rel">Добавить поле</div>

                    <form action="#" class="---form">
                        <div class="---field">
                            <label>Выберите раздел</label>
                            <div class="---select-2 ---select-2--color-acent">
                                <select class="---font-sbold">
                                    <option value="">Статус заявки 1</option>
                                    <option value="">Статус заявки 2</option>
                                    <option value="">Статус заявки 3</option>
                                    <option value="">Статус заявки 4</option>
                                    <option value="">Статус заявки 5</option>
                                </select>
                                
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>
                        </div>

                        <div class="---field">
                            <label>Выберите тип заявки</label>
                            <div class="---select-2">
                                <select class="---font-sbold">
                                    <option value="">На новые заявки 1</option>
                                    <option value="">На новые заявки 2</option>
                                    <option value="">На новые заявки 3</option>
                                    <option value="">На новые заявки 4</option>
                                    <option value="">На новые заявки 5</option>
                                </select>
                                
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>
                        </div>

                        <div class="---field">
                            <label>Название</label>
                            <input class="---input ---fill--bg" placeholder="Новый клиент">
                        </div>
                        
                        <div class="---field">
                            <label>впишите номер цвета</label>
                            <div class="---input ---input--choose-color ---fill--bg ---pos-rel">
                                <input placeholder="Пример: 0156ab">
                                <div class="---color-example ---y-pos-abs" style="background: #0156ab"></div>
                            </div>
                        </div>

                        <button class="---btn ---btn--fill-acent">Добавить</button>
                    </form>
                </div>
            </div>

        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>