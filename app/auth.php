<?php
    $pTitle = 'Title страницы';
    $pDes = 'Description страницы';
    $pImage = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://'. $_SERVER['HTTP_HOST'] . '/static/imgs/logo.png';
    $pUrl = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    ob_start();
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="ru"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="ru"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="ru"><![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
    <title><?=$pTitle;?></title>
    <meta name="description" content="<?=$pDes;?>">
    <meta name="keywords" content="">
    <meta name="robots" content="index, follow">

    <!-- Open Graph data -->
    <meta property="og:title" content="<?=$pTitle;?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=$pUrl;?>" />
    <meta property="og:image" content="<?=$pImage;?>" />
    <meta property="og:description" content="<?=$pDes;?>" />
    <meta property="og:site_name" content="" />
    <meta property="og:locale" content="ru_RU" />

    <!-- Twitter -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<?=$pTitle;?>"/>
    <meta name="twitter:description" content="<?=$pDes;?>"/>
    <meta name="twitter:image:src" content="<?=$pImage;?>"/>
    <meta name="twitter:site" content="<?=$pUrl;?>"/>

    <!-- Google Plus -->
    <meta itemprop="name" content="" />
    <meta itemprop="description" content="<?=$pDes;?>"/>
    <meta itemprop="image" content="<?=$pImage;?>"/>
    
    <!-- Подключение шрифтов -->
    <link rel="preload" href="/static/fonts/Gilroy/ExtraBold/Gilroy-ExtraBold.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/static/fonts/Gilroy/SemiBold/Gilroy-SemiBold.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/static/fonts/Gilroy/Regular/Gilroy-Regular.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/static/fonts/Gilroy/Light/Gilroy-Light.woff" as="font" type="font/woff" crossorigin>

    <!-- Стили -->
    <style><?php echo file_get_contents('static/styles/html_styles.css') ?></style>
    <link rel="stylesheet" href="/static/styles/main.css">

    <meta name="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="imagetoolbar" content="no">
    <meta http-equiv="msthemecompatible" content="no">
    <meta http-equiv="cleartype" content="on">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="address=no"/>
    <meta name="viewport" content="width=device-width,height=device-height, initial-scale=1.0, user-scalable=no,maximum-scale=1.0, minimal-ui"/>
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="static/imgs/favicon.ico">
</head>
<body class="---d-flex ---pos-rel">

    <div id="---page-auth" class="---d-flex">
        <div class="---text-wrap ---pos-rel ---d-flex ---justify-content-center ---flex-column">
            <div class="---logo ---d-flex ---align-items-center ---font-ebold">
                <img data-src="/static/imgs/logo.svg" alt="" class="---lazyload">
                <span>
                    stroika<br>
                    <span class="---acent">arenda</span>
                </span>
            </div>

            <div class="---title ---font-sbold">
                Добро<br>
                пожаловать!
            </div>

            <form action="#" class="---form">
                <div class="---field">
                    <input class="---input" placeholder="Ваш логин">
                </div>
                <div class="---field">
                    <input type="password" class="---input" placeholder="Пароль">
                </div>

                <button class="---btn ---btn--fill-acent">Войти в систему</button>
            </form>
        </div>

        <div class="---bg-img ---d-none ---xs-d-block">
            <img data-src="/static/imgs/bg/auth.png" alt="" class="---lazyload ---img-cover">
        </div>
    </div>

    <script>
        <?=file_get_contents('static/js/html_js.min.js'); ?>
        
        // requireResource('main.css', 'css', '1.2.5', '/static/styles/main.css');

    </script>
    <script defer src="/static/js/scripts.min.js"></script>

</body>
</html>
<?php  
    $out = ob_get_clean();
    $out = preg_replace('/(?![^<]*<\/pre>)[\n\r\t]+/', "\n", $out);
    $out = preg_replace('/ {2,}/', ' ', $out);
    $out = preg_replace('/>[\n]+/', '>', $out);
    echo $out;
?>