<?php require_once('layouts/header.php'); ?>

    <div class="---page-sale ---row">

        <div class="---col-12 ---col-md-7">
            <div class="---tabs ---tabs--finance">
                <div class="---tabs__switchers-wrapper ---d-flex ---align-items-center ---justify-content-between">
                    <div class="---tabs__switchers ---h1 ---font-light ---d-flex">
                        <div class="---tabs__switcher ---is-active" data-tab="1">Активные акции</div>
                        <div class="---tabs__switcher" data-tab="2">Архив</div>
                    </div>
                </div>

                <div class="---row">
                    <div class="---col-12 ---col-sm-6">
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---devider"></div>

                <div class="---tabs__tab ---is-visible" data-tab="1">
                    
                    <div class="---sale-list__item ---form ---radius-5 ---block--bg-white">
                        
                        <div class="---item__header ---pos-rel">
                            <div class="h3 ---font-sbold">Скидка 30% при аренде от 30 000 ₽</div>
                            
                            <div class="---icons ---d-flex ---y-pos-abs">
                                <a href="#" class="ifont ---icon-edit ---acent"></a>
                                <a href="#" class="ifont ---icon-close"></a>
                            </div>
                        </div>
                        <div class="---item__content ---row ---_sm">
                            <div class="---field col ---col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">Доступно всем клиентам</label>
                            </div>
                            <div class="---field col --col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">На все категории оборудования</label>
                            </div>
                            <div class="---field col ---col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">Действует автоматически</label>
                            </div>
                            <div class="---field col ---col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">Не суммуриуется с основной скидкой</label>
                            </div>
                        </div>

                    </div>
                    <div class="---sale-list__item ---form ---radius-5 ---block--bg-white">
                        
                        <div class="---item__header ---pos-rel">
                            <div class="h3 ---font-sbold">Скидка 30% при аренде от 30 000 ₽</div>
                            
                            <div class="---icons ---d-flex ---y-pos-abs">
                                <a href="#" class="ifont ---icon-edit ---acent"></a>
                                <a href="#" class="ifont ---icon-close"></a>
                            </div>
                        </div>
                        <div class="---item__content ---row ---_sm">
                            <div class="---field col ---col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">Доступно всем клиентам</label>
                            </div>
                            <div class="---field col --col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">На все категории оборудования</label>
                            </div>
                            <div class="---field col ---col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">Действует автоматически</label>
                            </div>
                            <div class="---field col ---col-12 ---col-sm-6">
                                <label class="---checkbox ---checkbox--dark">Не суммуриуется с основной скидкой</label>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="---tabs__tab" data-tab="2">

                    <div class="---sale-list__item ---radius-5 ---block--bg-white">
                        
                        <div class="---item__header ---pos-rel">
                            <div class="h3 ---font-sbold">Скидка 30% при аренде от 30 000 ₽</div>
                            
                            <div class="---icons ---d-flex ---y-pos-abs">
                                <a href="#" class="ifont ---icon-edit ---acent"></a>
                                <a href="#" class="ifont ---icon-close"></a>
                            </div>
                        </div>
                        <div class="---item__content ---row ---_sm">
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">Доступно всем клиентам</label>
                            </div>
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">На все категории оборудования</label>
                            </div>
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">Действует автоматически</label>
                            </div>
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">Не суммуриуется с основной скидкой</label>
                            </div>
                        </div>

                    </div>
                    <div class="---sale-list__item ---radius-5 ---block--bg-white">
                        
                        <div class="---item__header ---pos-rel">
                            <div class="h3 ---font-sbold">Скидка 30% при аренде от 30 000 ₽</div>
                            
                            <div class="---icons ---d-flex ---y-pos-abs">
                                <a href="#" class="ifont ---icon-edit ---acent"></a>
                                <a href="#" class="ifont ---icon-close"></a>
                            </div>
                        </div>
                        <div class="---item__content ---row ---_sm">
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">Доступно всем клиентам</label>
                            </div>
                            <div class="col --col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">На все категории оборудования</label>
                            </div>
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">Действует автоматически</label>
                            </div>
                            <div class="col ---col-12 ---col-md-6">
                                <label class="---checkbox ---checkbox--dark">Не суммуриуется с основной скидкой</label>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="---col-12 ---col-md-5">
            <div class="---r-side-add-block ---block--bg-white ---radius-5">
                <div class="h2 ---font-sbold">Добавить новую акцию</div>

                <div class="---border"></div>

                <div class="---form">
                    <div class="---field">
                        <label>Название акции</label>
                        <input class="---input ---fill--bg">
                    </div>
                    
                    <div class="---row">
                        <div class="col ---field ---col-12 ---col-xs-6">
                            <label>Размер скидки (%)</label>
                            <div class="---input ---check ---pos-rel">
                                <div class="---box ---y-pos-abs"></div>
                                <input class="---font-sbold" disabled>
                            </div>
                        </div>
                        <div class="col ---field ---col-12 ---col-xs-6">
                            <label>или сумма в рублях</label>
                            <div class="---input ---check ---pos-rel">
                                <div class="---box ---y-pos-abs"></div>
                                <input type="money" class="---font-sbold" disabled>
                            </div>
                        </div>

                        <div class="col ---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Выберите филиал</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col ---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Доступность акции</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col ---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Категории оборудования</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col ---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Действие акции</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col ---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Суммируется</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---border"></div>
                
                <a href="#" class="---btn ---btn--fill-acent">Добавить</a>
            </div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>