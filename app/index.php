<?php require_once('layouts/header.php'); ?>

    <div class="---block ---block--bg-white ---block-data-by-city ---d-flex ---justify-content-between">

        <div class="---block__lside ---pos-rel ---font-sbold">
            <i class="ifont ---icon-location ---y-pos-abs"></i>
            Данные<br>
            по городам
        </div>

        <div class="---block__rside ---d-flex">
            <a href="#" class="---city">Казань <span class="---icon ---y-pos-abs">-</span></a>
            <a href="#" class="---city ---city--add ---font-regular">Добавить <span class="---icon ---y-pos-abs">+</span></a>
        </div>

    </div>


    <div class="---block ---block-charts ---row">
            
        <div class="---col-12 ---col-sm-6 ---col-md-4">
            <div class="---chart-item ---chart-item--today">
                <div class="---graphic">
                    
                </div>
                <div class="---content ---d-flex ---justify-content-between ---align-items-center">
                    <div class="---title ---pos-rel ---font-sbold">
                        <i class="ifont ---icon-lighting ---y-pos-abs"></i>
                        Доход<br>
                        за сегодня
                    </div>

                    <div class="---num ---font-ebold">9 870 ₽</div>
                </div>
            </div>
        </div>
        <div class="---col-12 ---col-sm-6 ---col-md-4">
            <div class="---chart-item">
                <div class="---graphic">
                    
                </div>
                <div class="---content ---d-flex ---justify-content-between ---align-items-center">
                    <div class="---title ---pos-rel ---font-sbold">
                        <i class="ifont ---icon-calendar ---y-pos-abs"></i>
                        Доход<br>
                        за месяц
                    </div>

                    <div class="---num ---font-light">398 560 ₽</div>
                </div>
            </div>
        </div>
        <div class="---col-12 ---col-sm-6 ---col-md-4">
            <div class="---chart-item">
                <div class="---graphic">
                    
                </div>
                <div class="---content ---d-flex ---justify-content-between ---align-items-center">
                    <div class="---title ---pos-rel ---font-sbold">
                        <i class="ifont ---icon-basket-fill ---y-pos-abs"></i>
                        В аренде<br>
                        за месяц
                    </div>

                    <div class="---num ---font-light">392 позиций</div>
                </div>
            </div>
        </div>

    </div>


    <div class="---block ---block-index-stat ---row">

        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/1.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Конверсия<br> 
                    сайта
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    4,5%
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/2.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Новых<br> 
                    клиентов
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    19
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/3.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Постоянных<br> 
                    клиентов
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    73%
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/4.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Просроченные<br> 
                    в аренде
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    16
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/5.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    В ожидании<br> 
                    оплаты
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    7
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>
        <div class="---col-6 ---col-xs-4 ---col-xl-2">
            <div class="---stat-item">
                <div class="---stat-item__icon ---pos-rel">
                    <img data-src="/static/imgs/icons/index/6.svg" alt="" class="---lazyload ---x-pos-abs">
                </div>

                <div class="---stat-item__title">
                    Новых заявок<br> 
                    за сегодня
                </div>

                <div class="---num ---pos-rel ---font-ebold">
                    22
                    <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                </div>

                <div class="---stat-item__foter ---pos-rel">
                    За этот месяц
                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                </div>
            </div>
        </div>

    </div>


    <div class="---col-12 ---devider-line"></div>

    <div class="---block-catalog">
        <div class="---block__title ---font-light ---d-flex ---align-items-center ---justify-content-between">
            Популярные позиции
            <a href="#" class="---show-all ---font-sbold ---pos-rel">Показать все <i class="ifont ---icon-arrow-right ---y-pos-abs"></i></a>
        </div>

        <div class="---cat-wrapper ---row ---_xs">

            <?php for($i = 0; $i < 8; $i++): ?>
            <div class="---col col">
                <a href="#" class="---cat-item ---d-flex ---flex-column">
                    <span class="---cat-item__img">
                        <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                    </span>
                    <span class="---cat-item__title">
                        Отбойный молоток MAKITA HM1203C
                    </span>
                    <span class="---cat-item__orders">
                        <span class="---font-sbold">864</span> заказаов
                    </span>
                    <span class="---cat-item__btn ---font-sbold ---btn ---btn--sm">
                        <i class="ifont ---icon-basket-linear"></i>
                        от 750 ₽
                    </span>
                </a>
            </div>
            <?php endfor; ?>

        </div>
    </div>


    <div class="---col-12 ---devider-line"></div>

    <div class="---block-catalog">
        <div class="---block__title ---font-light ---d-flex ---align-items-center ---justify-content-between">
            <div class="---d-inline-flex ---flex-wrap ---align-items-center">
                Новинки каталога

                <div class="---select">
                    <select>
                        <option value="">Электроинструменты 1</option>
                        <option value="">Электроинструменты 2</option>
                        <option value="">Электроинструменты 3</option>
                        <option value="">Электроинструменты 4</option>
                        <option value="">Электроинструменты 5</option>
                    </select>

                    <div class="---select__cur-value">
                        <span>Электроинструменты</span>
                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                    </div>

                    <div class="---select__list-wrapper">
                        <div class="---select__list">
                            <a href="#" class="---select__list-item">Электроинструменты 1</a>
                            <a href="#" class="---select__list-item">Электроинструменты 2</a>
                            <a href="#" class="---select__list-item">Электроинструменты 3</a>
                            <a href="#" class="---select__list-item">Электроинструменты 4</a>
                            <a href="#" class="---select__list-item">Электроинструменты 5</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="---show-all ---font-sbold ---pos-rel">Показать все <i class="ifont ---icon-arrow-right ---y-pos-abs"></i></a>
        </div>

        <div class="---cat-wrapper ---row ---_xs">

            <?php for($i = 0; $i < 8; $i++): ?>
            <div class="---col col">
                <a href="#" class="---cat-item ---d-flex ---flex-column">
                    <span class="---cat-item__img">
                        <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                    </span>
                    <span class="---cat-item__title">
                        Отбойный молоток MAKITA HM1203C
                    </span>
                    <span class="---cat-item__orders">
                        <span class="---font-sbold">864</span> заказаов
                    </span>
                    <span class="---cat-item__btn ---font-sbold ---btn ---btn--sm">
                        <i class="ifont ---icon-basket-linear"></i>
                        от 750 ₽
                    </span>
                </a>
            </div>
            <?php endfor; ?>

        </div>
    </div>

<?php require_once('layouts/footer.php'); ?>