<?php require_once('layouts/header.php'); ?>

    <div class="---page-application">

        <div class="---tabs ---tabs--finance">
            <div class="---tabs__switchers-wrapper ---d-flex ---align-items-center ---justify-content-between">
                <div class="---tabs__switchers ---h1 ---font-light ---d-flex">
                    <div class="---tabs__switcher ---is-active" data-tab="1">Все заявки</div>
                    <div class="---tabs__switcher" data-tab="2">С сайта</div>
                    <div class="---tabs__switcher" data-tab="3">Соц. сети</div>
                    <div class="---tabs__switcher" data-tab="4">Соц. сети</div>
                </div>
            </div>

            <div class="---h-filters-wrapper ---d-flex ---justify-content-between ---align-items-start">
                    
                <div class="---d-flex ---form ---align-items-start">
                    <a href="#" class="---filters ---js-toggle-show ---radius-5 ---d-inline-flex ---align-items-center" data-block="#filter">
                        Фильтры
                        <i class="ifont ---icon-menu-finance ---d-none ---sm-d-flex"></i>
                    </a>

                    <a href="#" class="---filters ---js-toggle-show ---radius-5 ---d-inline-flex ---align-items-center" data-block="#active-field">
                        Активные поля
                        <i class="ifont ---icon-list ---d-none ---sm-d-flex"></i>
                    </a>

                    <div class="---search ---field ---pos-rel">
                        <input class="---input" placeholder="Поиск по клиентам">
                        <button class="---y-pos-abs ifont ---icon-loop"></button>
                    </div>
                </div>

                <a href="#" class="---btn ---btn--fill-acent">Добавить товар</a>

            </div>

            <div id="filter" class="---d-none ---block--bg-white ---block-filter ---radius-5 ---form">
                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---btns ---d-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Применить фильтры</a>
                    <a href="#" class="---btn ---btn--border-bg">
                        Сбросить фильры
                        <i class="ifont ---icon-close"></i>
                    </a>
                </div>
            </div>

            <div id="active-field" class="---d-none ---block--bg-white ---block-filter ---radius-5 ---form">
                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---btns ---d-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Применить фильтры</a>
                    <a href="#" class="---btn ---btn--border-bg">
                        Сбросить фильры
                        <i class="ifont ---icon-close"></i>
                    </a>
                </div>
            </div>

            <div class="---tabs__tab ---is-visible" data-tab="1">
                <div class="---t-wrapper">
                    <table>
                        <tr>
                            <th style="width: 46px;"><label class="---checkbox"></label></th>
                            <th>Клиент</th>
                            <th>Номер телефона</th>
                            <th>Что интересует</th>
                            <th>Результат</th>
                            <th>Источник</th>
                            <th>Комментарий менеджера</th>
                        </tr>
                        <?php for($i = 0; $i < 2; $i++): ?>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>Александр</td>
                            <td>+7 (999) 156-1992</td>
                            <td>Аренда оборудования</td>
                            <td>
                                <div class="---select ---purple ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>С сайта</td>
                            <td>Ждем ответа по наличию со склада</td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>Александр</td>
                            <td>+7 (999) 156-1992</td>
                            <td>Аренда оборудования</td>
                            <td>
                                <div class="---select ---green ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>С сайта</td>
                            <td>Ждем ответа по наличию со склада</td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td><div class="---font-sbold">Александр</div></td>
                            <td><div class="---font-sbold">+7 (999) 156-1992</div></td>
                            <td><div class="---font-sbold">Аренда оборудования</div></td>
                            <td>
                                <div class="---select ---red ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td><div class="---font-sbold">С сайта</div></td>
                            <td><div class="---font-sbold">Ждем ответа по наличию со склада</div></td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>Александр</td>
                            <td>+7 (999) 156-1992</td>
                            <td>Аренда оборудования</td>
                            <td>
                                <div class="---select ---dark ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>С сайта</td>
                            <td>Ждем ответа по наличию со склада</td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>Александр</td>
                            <td>+7 (999) 156-1992</td>
                            <td>Аренда оборудования</td>
                            <td>
                                <div class="---select ---gray ---bg-color">
                                    <select>
                                        <option value="">аренда помещения</option>
                                        <option value="">аренда оборудования</option>
                                        <option value="">ремонт</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>аренда помещения</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">аренда помещения</a>
                                            <a href="#" class="---select__list-item">аренда оборудования</a>
                                            <a href="#" class="---select__list-item">ремонт</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>С сайта</td>
                            <td>Ждем ответа по наличию со склада</td>
                        </tr>
                        <?php endfor; ?>
                    </table>
                </div>

                <div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">
                    <div class="---nums ---d-flex ---align-items-center">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            Далее
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>

                    <div class="---pagination__total-count">
                        Всего платежей — <span class="---font-sbold">8 521</span>
                    </div>
                </div>
            </div>
            <div class="---tabs__tab" data-tab="2"></div>
            <div class="---tabs__tab" data-tab="3"></div>
            <div class="---tabs__tab" data-tab="4"></div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>