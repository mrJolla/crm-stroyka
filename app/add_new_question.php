<?php require_once('layouts/header.php'); ?>

    <div class="---page-add-question ---row">
        
        <div class="---col-12 ---page-title">Заяка на спрос оборудования</div>

        <div class="---col-12 ---col-md-6 ---form">
            
            <div class="---form-wrapper">
                <div class="h2 ---font-sbold">Данные клиента</div>

                <div class="---row">
                    <div class="---col-12 ---col-xs-6 ---field">
                        <label>Имя</label>
                        <input class="---input">
                    </div>
                    <div class="---col-12 ---col-xs-6 ---field">
                        <label></label>
                        <div class="---add-client-from-base ---pos-rel ---radius-5">
                            Добавить клиента из базы
                            <i class="ifont ---icon-loop ---y-pos-abs ---d-none ---xl-d-flex"></i>
                        </div>
                    </div>
                    <div class="---col-12 ---col-xs-6 ---field">
                        <label>Электронная почта</label>
                        <input type="email" class="---input">
                    </div>
                    <div class="---col-12 ---col-xs-6 ---field">
                        <label>Номер телефона</label>
                        <input type="tel" class="---input">
                    </div>
                </div>
            </div>

            <div class="---form-wrapper">
                <div class="h2 ---font-sbold">Данные о запросе</div>

                <div class="---row">
                    <div class="---col-12 ---col-lg-6 ---field">
                        <label>Источник запроса</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---col-12 ---col-lg-6 ---field">
                        <label>Статус</label>
                        <div class="---select ---purple ---bg-color">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="---col-12 ---field">
                        <label>Комментарий менеджера</label>
                        <textarea rows="5" class="---input">Клиент жалуется на то что у нас в аренде всего 2 еденицы Makita HM1203C. А ему требуется минимум 4 и на длительный строк.</textarea>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="---col-12 ---col-md-6">
            
            <div class="---equipment-wrapper ---r-side-add-block ---block--bg-white ---radius-5">
                <div class="h2 ---font-sbold">Запрашиваемое оборудование</div>

                <div class="---prod-list">
                    <?php for($i = 0; $i < 3; $i++): ?>
                    <a href="#" class="---prod-item ---pos-rel ---d-flex ---align-items-center ---justify-content-between">
                        <span class="---prod-item__info ---d-inline-flex ---align-items-center">
                            <span class="---prod-item__img ---radius-5 ---d-flex ---align-items-center ---justify-content-center">
                                <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                            </span>
                            <span class="---prod-item__title ---font-sbold">Отбойный молоток<br> MAKITA HM1203C</span>
                        </span>

                        <span class="---prod-item__thumb ---d-inline-flex">
                            <span class="---thumb-item">
                                За единицу<br> 
                                <span class="---font-sbold">750 ₽</span>
                            </span>
                            <span class="---thumb-item">
                                В наличии<br> 
                                <span class="---font-sbold">0 шт.</span>
                            </span>
                        </span>

                        <span class="---prod-qnty-wrapper ---radius-5 ---d-inline-flex ---align-items-center">
                            <span class="---switcher ---d-inline-flex ---justify-content-center ---align-items-center">-</span>
                            <span class="---qnty ---font-sbold">1</span>
                            <span class="---switcher ---d-inline-flex ---justify-content-center ---align-items-center ---plus">+</span>
                        </span>
                    </a>
                    <?php endfor; ?>
                </div>

                <a href="#" class="---btn ---btn--fill-acent">Добавить</a>
            </div>

        </div>

        <div class="---col-12">
            <div class="---footer">
                <a href="#" class="---btn ---btn--fill-acent">Сохранить обращение</a>
            </div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>