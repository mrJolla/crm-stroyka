<?php require_once('layouts/header.php'); ?>    
    <div class="---page-rent">

        <div class="---tabs ---tabs--finance">
            <div class="---tabs__switchers-wrapper ---d-flex ---align-items-center ---justify-content-between">
                <div class="---tabs__switchers ---h1 ---font-light ---d-flex">
                    <div class="---tabs__switcher ---d-flex ---align-items-center ---is-active" data-tab="1">В прокате</div>
                    <div class="---tabs__switcher ---d-flex ---align-items-center" data-tab="2">
                        Новые заявки
                        <span class="---radius-5 ---d-flex ---align-items-center ---justify-content-center ---font-sbold ---m-color">3</span>
                    </div>
                    <div class="---tabs__switcher ---d-flex ---align-items-center" data-tab="3">
                        Просроченные
                        <span class="---radius-5 ---d-flex ---align-items-center ---justify-content-center ---font-sbold ---r-color">8</span>
                    </div>
                </div>
            </div>

            <div class="---h-filters-wrapper ---d-flex ---justify-content-between ---align-items-start">
                    
                <div class="---d-flex ---form ---align-items-start">
                    <a href="#" class="---filters ---js-toggle-show ---radius-5 ---d-inline-flex ---align-items-center" data-block="#filter">
                        Фильтры
                        <i class="ifont ---icon-menu-finance ---d-none ---sm-d-flex"></i>
                    </a>

                    <a href="#" class="---filters ---js-toggle-show ---radius-5 ---d-inline-flex ---align-items-center" data-block="#active-field">
                        Активные поля
                        <i class="ifont ---icon-list ---d-none ---sm-d-flex"></i>
                    </a>

                    <div class="---search ---field ---pos-rel">
                        <input class="---input" placeholder="Поиск по клиентам">
                        <button class="---y-pos-abs ifont ---icon-loop"></button>
                    </div>
                </div>

                <a href="#" class="---btn ---btn--fill-acent">Добавить товар</a>

            </div>

            <div id="filter" class="---d-none ---block--bg-white ---block-filter ---radius-5 ---form">
                <div class="---row">
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Статус заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Даты заказа</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Тип изделия</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-sm-6 ---col-lg-3">
                        <label>Менеджер</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="---btns ---d-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Применить фильтры</a>
                    <a href="#" class="---btn ---btn--border-bg">
                        Сбросить фильры
                        <i class="ifont ---icon-close"></i>
                    </a>
                </div>
            </div>

            <div id="active-field" class="---d-none ---block--bg-white ---block-filter ---radius-5 ---form">
                <div class="---row ---justify-content-between">
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Оборудование</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Начало</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Возврат</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Состояние</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Цена</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Цена со скидкой</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Остаток</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Оплата сегодня</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Дата создания</label>
                    </div>
                    <div class="---field ---col">
                        <label for="" class="---checkbox"><input> Срок аренды</label>
                    </div>
                </div>

                <div class="---btns ---d-flex">
                    <a href="#" class="---btn ---btn--fill-acent">Обновить поля</a>
                    <a href="#" class="---btn ---btn--border-bg">
                        Вернуть к стандарту
                        <i class="ifont ---icon-close"></i>
                    </a>
                </div>
            </div>

            <div class="---tabs__tab ---is-visible" data-tab="1">

                <div class="---t-wrapper">
                    <table>
                        <tr>
                            <th><label class="---checkbox"></label></th>
                            <th>Категория</th>
                            <th>Оборудование</th>
                            <th>Доступность</th>
                            <th>на складе</th>
                            <th>Закуп цена</th>
                            <th>Остаточ цена</th>
                            <th>Цена за сутки</th>
                            <th>Прокатов</th>
                            <th>Поступил</th>
                            <th>ремонтов</th>
                        </tr>
                        <?php for($i = 0; $i < 5; $i++): ?>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>Электроинструменты</td>
                            <td>Отбойный молоток<br> Makita HM1202C</td>
                            <td>
                                <div class="---select ---purple ---bg-color">
                                    <select>
                                        <option value="">Электроинструменты 1</option>
                                        <option value="">Электроинструменты 2</option>
                                        <option value="">Электроинструменты 3</option>
                                        <option value="">Электроинструменты 4</option>
                                        <option value="">Электроинструменты 5</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>Электроинструменты</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td><div class="---font-sbold">4 шт.</div></td>
                            <td><div class="---font-sbold">28 000 ₽</div></td>
                            <td><div class="---font-sbold">15 200 ₽</div></td>
                            <td><div class="---font-sbold">1 500 ₽</div></td>
                            <td><div class="---font-sbold">242</div></td>
                            <td>28.01.2017</td>
                            <td><div class="---font-sbold">0</div></td>
                        </tr>
                        <tr>
                            <td><label class="---checkbox"></label></td>
                            <td>Электроинструменты</td>
                            <td>Отбойный молоток<br> Makita HM1202C</td>
                            <td>
                                <div class="---select ---green ---bg-color">
                                    <select>
                                        <option value="">Электроинструменты 1</option>
                                        <option value="">Электроинструменты 2</option>
                                        <option value="">Электроинструменты 3</option>
                                        <option value="">Электроинструменты 4</option>
                                        <option value="">Электроинструменты 5</option>
                                    </select>

                                    <div class="---select__cur-value">
                                        <span>Электроинструменты</span>
                                        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                    </div>

                                    <div class="---select__list-wrapper">
                                        <div class="---select__list">
                                            <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                            <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td><div class="---font-sbold">4 шт.</div></td>
                            <td><div class="---font-sbold">28 000 ₽</div></td>
                            <td><div class="---font-sbold">15 200 ₽</div></td>
                            <td><div class="---font-sbold">1 500 ₽</div></td>
                            <td><div class="---font-sbold">242</div></td>
                            <td>28.01.2017</td>
                            <td><div class="---font-sbold">0</div></td>
                        </tr>
                        <?php endfor; ?>
                    </table>
                </div>
                
                <div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">
                    <div class="---nums ---d-flex ---align-items-center">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            Далее
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>

                    <div class="---pagination__total-count">
                        Всего платежей — <span class="---font-sbold">8 521</span>
                    </div>
                </div>

            </div>
        </div>

    </div>
<?php require_once('layouts/footer.php'); ?>