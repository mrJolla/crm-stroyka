<?php require_once('layouts/header.php'); ?>
    <div class="---page-client-in ---row">

        <div class="---col-12 ---col-lg-6 ---form">
            
            <div class="h1 ---font-light">Отбойный молоток Makita HM1202C</div>

            <div class="---devider"></div>

            <div class="---data-block">
                <div class="h2 ---font-sbold">О товаре</div>
                
                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Категория</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Тип инструмента</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Марка</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>номер инструмента</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Состояние товара</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Состояние товара</label>
                        <input type="money" class="---input">
                    </div>
                </div>
            </div>

            <div class="---data-block">
                <div class="h2 ---font-sbold">Данные на аренду</div>
                
                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Точка выдачи</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Износ оборудования</label>
                        <div class="---input ---input--wear ---font-sbold ---d-flex ---align-items-center ---justify-content-between">
                            86 %

                            <div class="---d-flex">
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot ---dot--acent"></div>
                                <div class="---dot"></div>
                                <div class="---dot"></div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Цена аренды за сутки</label>
                        <input type="money" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Максимальная скидка</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Цена аренды за 2-7 суток</label>
                        <input type="money" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Цена аренды за 8-14 сут</label>
                        <input type="money" class="---input">
                    </div>
                </div>
            </div>

            <div class="---data-block">
                <div class="h2 ---font-sbold">Описание товара на сайте</div>
                
                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label></label>
                        <label class="---pos-rel ---input ---input--file-upload">
                            <i class="ifont ---icon-camera ---y-pos-abs"></i>
                            <input type="file" class="---d-none" multiple>
                            <span class="---input__file-name">Загрузить фотографию</span>
                        </label>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Энергия удара, Дж</label>
                        <input type="tel" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Габаритная длина, мм</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Сетевой шнур, м</label>
                        <input type="tel" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Мощность, Вт</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <label>Частота ударов, уд/мин</label>
                        <input type="tel" class="---input">
                    </div>

                    <div class="---field ---col-12 ---col-xs-6 ---col-md-4 ---col-lg-6">
                        <a href="#" class="---input ---input--add-field ---d-block">Добавить новое поле</a>
                    </div>
                </div>
            </div>

            <div class="---btns-wrapper">
                <a href="#" class="---btn ---btn--fill-acent">Сохранить</a>
                <a href="#" class="---btn ---btn--fill-white">Списать</a>
            </div>

        </div>
        <div class="---col-12 ---col-lg-6">
            
            <div class="---r-side-add-block ---block--bg-white ---radius-5 ---form">
                <div class="h2 ---font-sbold">История использования</div>

                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Данные в период от</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Данные в период до</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="---devider ---devider-mt0"></div>
                
                <div class="---t-wrapper">
                    <table>
                        <tr>
                            <th>Дата</th>
                            <th style="width: 60%;">Аренда</th>
                            <th>Приход</th>
                        </tr>
                        <?php for($i = 0; $i < 7; $i++): ?>
                        <tr>
                            <td>24.09.2018 / 15:34</td>
                            <td>Отбойный молоток Makita HM1202C</td>
                            <td>1 300 ₽</td>
                        </tr>
                        <?php endfor; ?>
                    </table>
                </div>
                
                <div class="---block-pagination">

                    <div class="---nums ---d-flex ---align-items-center ---justify-content-between ---sm-justify-content-start">
                        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
                        <ul class="---d-flex ---font-sbold">
                            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">36</a></li>
                        </ul>
                        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
                            <span class="---d-none ---xs-d-block">Далее</span>
                            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
                        </a>
                    </div>
                </div>

                <div class="---devider"></div>

                <div class="---stat-wrapper ---d-flex ---justify-content-between">
                    <div class="---stat-item">
                        <div class="---stat__icon ---radius-5 ---pos-rel">
                            <img data-src="/static/imgs/icons/client-in/1.svg" alt="" class="---lazyload ---x-pos-abs">
                        </div>
                        <div class="---stat__title">
                            Арендован за<br> указанный период
                        </div>
                        <div class="---stat__num ---font-ebold">287</div>
                    </div>
                    <div class="---stat-item">
                        <div class="---stat__icon ---radius-5 ---pos-rel">
                            <img data-src="/static/imgs/icons/client-in/2.svg" alt="" class="---lazyload ---x-pos-abs">
                        </div>
                        <div class="---stat__title">
                            Cумма дохода за<br> указанный период
                        </div>
                        <div class="---stat__num ---font-ebold">90 000 ₽</div>
                    </div>
                    <div class="---stat-item">
                        <div class="---stat__icon ---radius-5 ---pos-rel">
                            <img data-src="/static/imgs/icons/client-in/3.svg" alt="" class="---lazyload ---x-pos-abs">
                        </div>
                        <div class="---stat__title">
                            Расходы за<br> весь период
                        </div>
                        <div class="---stat__num ---font-ebold">0 ₽</div>
                    </div>
                </div>

            </div>

        </div>

    </div>
<?php require_once('layouts/footer.php'); ?>