<?php require_once('layouts/header.php'); ?>
    <div class="---page-rent-active ---row">

        <div class="---col-12 ---col-lg-6 ---form">
            
            <div class="h1 ---font-light">Аренда ИП Карамазов / № 4553 7063</div>

            <div class="---devider"></div>

            <div class="---data-block">
                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Вид аренды</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Статус</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Начало аренды</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Окончание аренды</label>
                        <div class="---input ---input--calendar ---pos-rel">
                            <input type="datepicker" class="---radius-5">
                            <i class="---y-pos-abs ifont ---icon-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="---data-block">
                <div class="h2 ---font-sbold ---d-flex ---flex-wrap ---align-items-center ---justify-content-between">
                    Данные арендатора

                    <a href="#" class="---check-by-bailiff">Проверить через сервис приставов</a>
                </div>
                
                <div class="---row">
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>ФИО клиента</label>
                        <input class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Номер телефона</label>
                        <input type="tel" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Электронная почта</label>
                        <input type="email" class="---input">
                    </div>
                    <div class="---field ---col-12 ---col-xs-6">
                        <label>Город</label>
                        <input class="---input">
                    </div>

                    <div class="---prod-list">
                        
                    </div>
                </div>
            </div>

            <div class="---data-block">
                <div class="h2 ---font-sbold ---d-flex ---flex-wrap ---align-items-center ---justify-content-between">Оборудование</div>

                <div class="---prod-list">
                    <?php for($i = 0; $i < 3; $i++): ?>
                    <div href="#" class="---prod-item ---pos-rel ---d-flex ---align-items-center ---justify-content-between">
                        <span class="---prod-item__info ---d-inline-flex ---align-items-center">
                            <span class="---prod-item__img ---radius-5 ---d-flex ---align-items-center ---justify-content-center">
                                <img data-src="/static/imgs/catalog/1.png" alt="" class="---lazyload ---img-contain">
                            </span>
                            <span class="---prod-item__title ---font-sbold">Отбойный молоток<br> MAKITA HM1203C</span>
                        </span>

                        <span class="---prod-item__thumb ---d-inline-flex">
                            <span class="---thumb-item">
                                За единицу<br> 
                                <span class="---font-sbold">750 ₽</span>
                            </span>
                            <span class="---thumb-item">
                                В наличии<br> 
                                <span class="---font-sbold">0 шт.</span>
                            </span>
                        </span>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>

            <div class="---btns-wrapper">
                <a href="#" class="---btn ---btn--fill-acent">Создать заявку</a>
            </div>

        </div>
        <div class="---col-12 ---col-lg-6">
            
            <div class="---r-side-add-block ---equipment-wrapper ---block--bg-white ---radius-5 ---form">
                <div class="h2 ---font-sbold ---d-flex ---align-items-center ---justify-content-between">
                    История операций
                    <div class="---font-regular">Стоимость  18 800 ₽</div>
                </div>

                <div class="---devider"></div>

                <div class="---t-wrapper">
                    <table>
                        <tr>
                            <th>Дата</th>
                            <th style="width: 50%;">Аренда</th>
                            <th>Приход</th>
                        </tr>
                        <?php for($i = 0; $i < 8; $i++): ?>
                        <tr>
                            <td>24.09.2018 / 15:34</td>
                            <td>Оплата аренды</td>
                            <td>10 000 ₽</td>
                        </tr>
                        <?php endfor; ?>
                    </table>
                </div>

                <div class="---total ---font-sbold ---d-flex ---flex-wrap ---justify-content-between">
                    Итого внесено
                    <span>17 300 ₽</span>
                </div>
                <div class="---total ---total--red ---font-sbold ---d-flex ---flex-wrap ---justify-content-between">
                    Остается должен
                    <span>1 500 ₽</span>
                </div>

                <a href="#" class="---btn ---btn--fill-acent">Добавить платеж</a>
            </div>
            <div class="---r-side-add-block ---equipment-wrapper ---block--bg-white ---radius-5 ---form">
                <div class="h2 ---font-sbold">Действия по аренде</div>

                <div class="---devider"></div>

                <div class="---row">
                    <div class="---col-12 ---col-xs-6">
                        <label for="" class="---checkbox"><input> Оборудование возвращено</label>
                    </div>
                    <div class="---col-12 ---col-xs-6">
                        <label for="" class="---checkbox"><input> Оборудование у клиента</label>
                    </div>
                </div>

                <div class="---devider"></div>

                <div class="---btns ---d-flex ---flex-wrap">
                    <a href="#" class="---btn ---btn--fill-acent">Продлить аренду</a>
                    <a href="#" class="---btn ---btn--fill-bg">Завершить аренду</a>
                </div>
            </div>

        </div>

    </div>
<?php require_once('layouts/footer.php'); ?>