<?php require_once('layouts/header.php'); ?>

    <div class="---page-settings ---row ---justify-content-between">

        <div class="---col-12 ---col-xl-7">
            <div class="---page-switchers h1 ---font-light">
                <a href="#">Печатные формы</a>
                <a href="#">Акции</a>
                <a href="#" class="---is-active">Доступ</a>
            </div>

            <div class="---row">
                <div class="---col-12 ---col-sm-6">
                    <div class="---select">
                        <select>
                            <option value="">Электроинструменты 1</option>
                            <option value="">Электроинструменты 2</option>
                            <option value="">Электроинструменты 3</option>
                            <option value="">Электроинструменты 4</option>
                            <option value="">Электроинструменты 5</option>
                        </select>

                        <div class="---select__cur-value">
                            <span>Электроинструменты</span>
                            <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                        </div>

                        <div class="---select__list-wrapper">
                            <div class="---select__list">
                                <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                <a href="#" class="---select__list-item">Электроинструменты 5</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="---devider"></div>

            <div class="---docs-wrapper">
                <div class="h2 ---font-sbold">Администраторы</div>

                <div class="---doc-item ---doc-item--avatar ---pos-rel ---radius-5">
                    <i class="---icon ifont ---icon-head-linear ---y-pos-abs"></i>

                    <div class="---title ---font-sbold">Александр Новиков</div>
                    <div class="---date">+7 (999) 156-1992 / AlexNow@mail.com</div>

                    <div class="---icons ---y-pos-abs ---d-flex">
                        <a href="#" class="---history ---d-inline-flex ---align-items-center">История</a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
                <div class="---doc-item ---doc-item--avatar ---pos-rel ---radius-5">
                    <i class="---icon ifont ---icon-head-linear ---y-pos-abs"></i>

                    <div class="---title ---font-sbold">Александр Новиков</div>
                    <div class="---date">+7 (999) 156-1992 / AlexNow@mail.com</div>

                    <div class="---icons ---y-pos-abs ---d-flex">
                        <a href="#" class="---history ---d-inline-flex ---align-items-center">История</a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
            </div>

            <div class="---docs-wrapper">
                <div class="h2 ---font-sbold">Менеджеры</div>

                <div class="---doc-item ---doc-item--avatar ---pos-rel ---radius-5">
                    <i class="---icon ifont ---icon-phone-linear ---y-pos-abs"></i>

                    <div class="---title ---font-sbold">Александр Новиков</div>
                    <div class="---date">+7 (999) 156-1992 / AlexNow@mail.com</div>

                    <div class="---icons ---y-pos-abs ---d-flex">
                        <a href="#" class="---history ---d-inline-flex ---align-items-center">История</a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
                <div class="---doc-item ---doc-item--avatar ---pos-rel ---radius-5">
                    <i class="---icon ifont ---icon-head-linear ---y-pos-abs"></i>

                    <div class="---title ---font-sbold">Александр Новиков</div>
                    <div class="---date">+7 (999) 156-1992 / AlexNow@mail.com</div>

                    <div class="---icons ---y-pos-abs ---d-flex">
                        <a href="#" class="---history ---d-inline-flex ---align-items-center">История</a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="---col-12 ---col-xl-4">
            <div class="---r-side-add-block ---block--bg-white ---radius-5">
                <div class="h2 ---font-sbold">Добавить сотрудника</div>

                <div class="---border"></div>

                <div class="---form">
                    <div class="---row">
                        <div class="---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Выберите филиал</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="---field ---col-12 ---col-xs-6 ---col-xl-12">
                            <label>Выберите роль</label>
                            <div class="---select">
                                <select>
                                    <option value="">Электроинструменты 1</option>
                                    <option value="">Электроинструменты 2</option>
                                    <option value="">Электроинструменты 3</option>
                                    <option value="">Электроинструменты 4</option>
                                    <option value="">Электроинструменты 5</option>
                                </select>

                                <div class="---select__cur-value">
                                    <span>Электроинструменты</span>
                                    <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                                </div>

                                <div class="---select__list-wrapper">
                                    <div class="---select__list">
                                        <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                        <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="---border ---border-mt0"></div>

                    <div class="---row ---_sm">
                        <div class="---field col ---col-12 ---col-xs-6 ---col-md-4 ---col-xl-6">
                            <label>Имя</label>
                            <input class="---input ---fill--bg">
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6 ---col-md-4 ---col-xl-6">
                            <label>Фамилия</label>
                            <input class="---input ---fill--bg">
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6 ---col-md-4 ---col-xl-6">
                            <label>Логин</label>
                            <input class="---input ---fill--bg">
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6 ---col-md-4 ---col-xl-6">
                            <label>Пароль</label>
                            <input class="---input ---fill--bg">
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6 ---col-md-4 ---col-xl-6">
                            <label>Номер телфона</label>
                            <input type="tel" class="---input ---fill--bg">
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6 ---col-md-4 ---col-xl-6">
                            <label>E-mail</label>
                            <input class="---input ---fill--bg">
                        </div>
                    </div>

                    <div class="---border ---border-mt0"></div>

                    <div class="---row ---_sm">
                        <div class="---field col ---col-12 ---col-xs-6">
                            <label for="" class="---checkbox"><input> Оплата по счетам</label>
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6">
                            <label for="" class="---checkbox"><input> Добавление товаров</label>
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6">
                            <label for="" class="---checkbox"><input> Добавление товаров</label>
                        </div>
                        <div class="---field col ---col-12 ---col-xs-6">
                            <label for="" class="---checkbox"><input> Оплата по счетам</label>
                        </div>
                    </div>
                </div>

                <div class="---border ---border-mt0"></div>
                
                <a href="#" class="---btn ---btn--fill-acent">Добавить</a>
            </div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>