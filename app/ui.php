<?php require_once('layouts/header.php'); ?>

<div class="---form">
	

	<div class="h1">текст h1</div>
	<div class="h2">текст h2</div>
	<div class="h3">текст h3</div>
	<p>
		Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.
	</p>

	<!-- <div class="---select-2">
	    <select>
	        <option value="">Электроинструменты 1</option>
	        <option value="">Электроинструменты 2</option>
	        <option value="">Электроинструменты 3</option>
	        <option value="">Электроинструменты 4</option>
	        <option value="">Электроинструменты 5</option>
	    </select>
        
        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
	</div> -->

    <div class="---select">
	    <select>
	        <option value="">Электроинструменты 1</option>
	        <option value="">Электроинструменты 2</option>
	        <option value="">Электроинструменты 3</option>
	        <option value="">Электроинструменты 4</option>
	        <option value="">Электроинструменты 5</option>
	    </select>

	    <div class="---select__cur-value">
	        <span>Электроинструменты</span>
	        <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
	    </div>

	    <div class="---select__list-wrapper">
	        <div class="---select__list">
	            <a href="#" class="---select__list-item">Электроинструменты 1</a>
	            <a href="#" class="---select__list-item">Электроинструменты 2</a>
	            <a href="#" class="---select__list-item">Электроинструменты 3</a>
	            <a href="#" class="---select__list-item">Электроинструменты 4</a>
	            <a href="#" class="---select__list-item">Электроинструменты 5</a>
	        </div>
	    </div>
	</div>

	<a href="#" class="---btn ---btn--fill-acent">Кнопка стандартная</a>
	<a href="#" class="---btn ---btn--fill-acent ---btn--sm">Кнопка маленькая</a>

	<div class="---field">
        <div class="---input ---input--calendar ---pos-rel">
            <input type="datepicker" class="---radius-5" data-position="bottom left">
            <i class="---y-pos-abs ifont ---icon-calendar"></i>
        </div>
    </div>

	<div class="---form">
		<div class="---field">
			
			<label>Описание</label>
			<input class="---input" placeholder="Описание">

		</div>

		<div class="---field ---is-error">
			
			<label>Описание</label>
			<input class="---input" placeholder="Описание">

		</div>
	</div>

	<br>
	<br>
	<br>

	<label for="" class="---checkbox"><input> Чекбокс 1</label>
	<label for="" class="---checkbox"><input> Чекбокс 2</label>
	<label for="" class="---checkbox ---checkbox--radio"><input> Радио кнопка 1</label>
	<label for="" class="---checkbox ---checkbox--radio"><input> Радио кнопка 2</label>
	<label for="" class="---checkbox ---checkbox--radio"><input> Радио кнопка 3</label>


	<div class="---block--bg-white ---block-pagination ---d-flex ---align-items-center ---justify-content-between ---radius-5">
	    <div class="---nums ---d-flex ---align-items-center">
	        <a href="#" class="---button ---radius-5 ifont ---icon-arrow-left"></a>
	        <ul class="---d-flex ---font-sbold">
	            <li><a href="#" class="---radius-5 ---is-active">1</a></li>
	            <li><a href="#">2</a></li>
	            <li><a href="#">3</a></li>
	            <li><a href="#">4</a></li>
	            <li><a href="#">5</a></li>
	            <li><span>...</span></li>
	            <li><a href="#">36</a></li>
	        </ul>
	        <a href="#" class="---button ---radius-5 ---pos-rel ---next ---d-inline-flex ---align-items-center">
	            Далее
	            <i class="ifont ---icon-arrow-right ---y-pos-abs"></i>
	        </a>
	    </div>

	    <div class="---pagination__total-count">
	        Всего платежей — <span class="---font-sbold">8 521</span>
	    </div>
	</div>
</div>

<?php require_once('layouts/footer.php'); ?>