<?php
    $pTitle = 'Title страницы';
    $pDes = 'Description страницы';
    $pImage = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://'. $_SERVER['HTTP_HOST'] . '/static/imgs/logo.png';
    $pUrl = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    ob_start();
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="ru"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="ru"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="ru"><![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
    <title><?=$pTitle;?></title>
    <meta name="description" content="<?=$pDes;?>">
    <meta name="keywords" content="">
    <meta name="robots" content="index, follow">

    <!-- Open Graph data -->
    <meta property="og:title" content="<?=$pTitle;?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=$pUrl;?>" />
    <meta property="og:image" content="<?=$pImage;?>" />
    <meta property="og:description" content="<?=$pDes;?>" />
    <meta property="og:site_name" content="" />
    <meta property="og:locale" content="ru_RU" />

    <!-- Twitter -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<?=$pTitle;?>"/>
    <meta name="twitter:description" content="<?=$pDes;?>"/>
    <meta name="twitter:image:src" content="<?=$pImage;?>"/>
    <meta name="twitter:site" content="<?=$pUrl;?>"/>

    <!-- Google Plus -->
    <meta itemprop="name" content="" />
    <meta itemprop="description" content="<?=$pDes;?>"/>
    <meta itemprop="image" content="<?=$pImage;?>"/>
    
    <!-- Подключение шрифтов -->
    <link rel="preload" href="/static/fonts/Gilroy/ExtraBold/Gilroy-ExtraBold.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/static/fonts/Gilroy/SemiBold/Gilroy-SemiBold.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/static/fonts/Gilroy/Regular/Gilroy-Regular.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/static/fonts/Gilroy/Light/Gilroy-Light.woff" as="font" type="font/woff" crossorigin>

    <!-- Стили -->
    <style><?php echo file_get_contents('static/styles/html_styles.css') ?></style>
    <link rel="stylesheet" href="/static/styles/main.css">

    <meta name="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="imagetoolbar" content="no">
    <meta http-equiv="msthemecompatible" content="no">
    <meta http-equiv="cleartype" content="on">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="address=no"/>
    <meta name="viewport" content="width=device-width,height=device-height, initial-scale=1.0, user-scalable=no,maximum-scale=1.0, minimal-ui"/>
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="static/imgs/favicon.ico">
</head>
<body class="---d-flex ---pos-rel">

    <aside class="---left-main-menu">
        <a href="/" class="---logo ---pos-rel ---d-block">
            <img data-src="/static/imgs/logo.svg" alt="" class="---lazyload ---y-pos-abs">
            stroika
            <span class="---d-block">arenda</span>
        </a>

        <nav>
            <a href="/" class="---is-active">
                <i class="ifont ---icon-menu-dashboard ---y-pos-abs"></i>
                Рабочий стол
            </a>
            <a href="/rent.php">
                <i class="ifont ---icon-menu-rent ---y-pos-abs"></i>
                Прокат
            </a>
            <a href="/base.php">
                <i class="ifont ---icon-menu-base ---y-pos-abs"></i>
                Склад
            </a>
            <a href="/clients.php">
                <i class="ifont ---icon-menu-clients ---y-pos-abs"></i>
                Клиенты
            </a>
            <a href="/reports.php">
                <i class="ifont ---icon-menu-report ---y-pos-abs"></i>
                Отчеты
            </a>
            <a href="/settings.php">
                <i class="ifont ---icon-menu-settings ---y-pos-abs"></i>
                Настройки
            </a>
            <a href="/finance.php">
                <i class="ifont ---icon-menu-finance ---y-pos-abs"></i>
                Финансы
            </a>
            <a href="/orders.php">
                <i class="ifont ---icon-menu-orders ---y-pos-abs"></i>
                Заявки
            </a>
        </nav>

        <div class="---btns ---d-block ---sm-d-none">
            <div class="---in-rent">
                В аренде: <b class="---font-sbold">284</b>
            </div>

            <a href="#" class="---btn ---btn--fill-acent ---btn--sm">Создать заявку</a>
        </div>
    </aside>

    <div id="---main-content">
		
        <header class="---d-flex ---justify-content-end ---xl-justify-content-between ---page-header ---pos-rel">

            <div class="---search-form ---d-flex">
                <button class="ifont ---icon-loop ---y-pos-abs ---search__btn"></button>
                <input class="---search__input" placeholder="Поиск...">
                <i class="---js-close-search-form ifont ---icon-close ---y-pos-abs"></i>
            </div>
            
            <div id="---menu-trigger" class="---d-flex ---lg-d-none ---align-content-center ---justify-content-center ---y-pos-abs">
                <span></span>
            </div>

            <div class="---page-title ---align-self-center ---d-none ---xl-d-block">Рабочий стол</div>

            <div class="---d-flex">
                <a href="#" class="---h-block ---d-inline-flex ---align-items-center ---search ---js-open-search-form">
                    <span class="---h-block__title ---d-none ---xl-d-block">Поиск</span>
                    <i class="ifont ---icon-loop"></i>
                </a>

                <div class="---h-block ---d-none ---sm-d-inline-flex ---align-items-center ---in-rent">
                    <div class="---h-block__title ---d-none ---xl-d-block">В аренде</div>
                    <i class="ifont ---icon-electro-plug"></i>
                    <span class="---font-sbold">284</span>
                </div>

                <a href="#" class="---h-block ---d-inline-flex ---align-items-center ---notifications">
                    <i class="ifont ---icon-bell"></i>
                    <span class="---h-block__title ---d-none ---xl-d-block">Уведомления</span>
                    <span class="---font-sbold ---acent ---d-inline-flex ---align-items-center ---justify-content-center">1</span>
                </a>

                <div class="---h-block ---d-inline-flex ---align-items-center ---in-rent">
                    <div class="---h-block__title ---d-none ---xl-d-block">Продажи</div>
                    <i class="ifont ---icon-database ---d-none ---sm-d-inline-flex"></i>
                    <span class="---font-sbold">810 560 ₽</span>
                </div>



                <a href="#" class="---exit ---h-block ifont ---icon-exit"></a>
                <a href="#" class="---btn ---btn--fill-acent ---d-none ---sm-d-block">
                    Создать заявку
                </a>
            </div>

        </header>

        <div class="---main-wrapper">
