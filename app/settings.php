<?php require_once('layouts/header.php'); ?>

    <div class="---page-settings ---row ---justify-content-between">

        <div class="---col-12 ---col-xl-7">
            <div class="---page-switchers h1 ---font-light">
                <a href="#" class="---is-active">Печатные формы</a>
                <a href="#">Акции</a>
                <a href="#">Доступ</a>
            </div>

            <div class="---docs-wrapper">
                <div class="h2 ---font-sbold">Документы с арендатором</div>

                <div class="---doc-item ---pos-rel ---radius-5">
                    <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                    <div class="---date">Обновлен 26.04.2019</div>

                    <div class="---icons ---y-pos-abs">
                        <a href="#" class="ifont ---icon-sunglass"></a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
                <div class="---doc-item ---pos-rel ---radius-5">
                    <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                    <div class="---date">Обновлен 26.04.2019</div>

                    <div class="---icons ---y-pos-abs">
                        <a href="#" class="ifont ---icon-sunglass"></a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
            </div>

            <div class="---docs-wrapper">
                <div class="h2 ---font-sbold">Счета на оплату</div>

                <div class="---doc-item ---pos-rel ---radius-5">
                    <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                    <div class="---date">Обновлен 26.04.2019</div>

                    <div class="---icons ---y-pos-abs">
                        <a href="#" class="ifont ---icon-sunglass"></a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
                <div class="---doc-item ---pos-rel ---radius-5">
                    <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                    <div class="---date">Обновлен 26.04.2019</div>

                    <div class="---icons ---y-pos-abs">
                        <a href="#" class="ifont ---icon-sunglass"></a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
            </div>

            <div class="---docs-wrapper">
                <div class="h2 ---font-sbold">Акт оказания услуг</div>

                <div class="---doc-item ---pos-rel ---radius-5">
                    <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                    <div class="---date">Обновлен 26.04.2019</div>

                    <div class="---icons ---y-pos-abs">
                        <a href="#" class="ifont ---icon-sunglass"></a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
                <div class="---doc-item ---pos-rel ---radius-5">
                    <div class="---title ---font-sbold">Договор-Заявка с Заказщиком</div>
                    <div class="---date">Обновлен 26.04.2019</div>

                    <div class="---icons ---y-pos-abs">
                        <a href="#" class="ifont ---icon-sunglass"></a>
                        <a href="#" class="ifont ---icon-edit ---acent"></a>
                        <a href="#" class="ifont ---icon-close"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="---col-12 ---col-xl-4">
            <div class="---r-side-add-block ---block--bg-white ---radius-5">
                <div class="h2 ---font-sbold">Добавить новый документ</div>

                <div class="---border"></div>

                <div class="---form ---row">
                    <div class="---field ---col-12 ---col-xs-6 ---col-xl-12">
                        <label>Выберите раздел</label>
                        <div class="---select">
                            <select>
                                <option value="">Электроинструменты 1</option>
                                <option value="">Электроинструменты 2</option>
                                <option value="">Электроинструменты 3</option>
                                <option value="">Электроинструменты 4</option>
                                <option value="">Электроинструменты 5</option>
                            </select>

                            <div class="---select__cur-value">
                                <span>Электроинструменты</span>
                                <i class="ifont ---icon-arrow-down ---y-pos-abs"></i>
                            </div>

                            <div class="---select__list-wrapper">
                                <div class="---select__list">
                                    <a href="#" class="---select__list-item">Электроинструменты 1</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 2</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 3</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 4</a>
                                    <a href="#" class="---select__list-item">Электроинструменты 5</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="---field ---col-12 ---col-xs-6 ---col-xl-12">
                        <label>Название документа</label>
                        <input class="---input ---fill--bg" placeholder="Договор...">
                    </div>
                </div>

                <div class="---border ---border-mt0"></div>
                
                <a href="#" class="---btn ---btn--fill-acent">Добавить</a>
            </div>
        </div>

    </div>

<?php require_once('layouts/footer.php'); ?>